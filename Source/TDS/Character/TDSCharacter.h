// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FunctionLibrary/Types.h"
#include "TDSCharacter.generated.h"

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATDSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:
UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EMovementState MovementState = EMovementState::Run_State;
UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FCharacterSpeed MovementInfo;
UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector FWD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float deltaYaw;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float Yaw;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float timerDelay = 0.01;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool timerEnable = false;

	UFUNCTION()
		void InputAxisX(float Value); 
	UFUNCTION()
		void InputAxisY(float Value);
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float AxisX = 0.f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float AxisY = 0.f;
	// Tick Func
	UFUNCTION()
		void MovementTick(float DeltaTime);

	UFUNCTION(BlueprintNativeEvent)
		void TRunRotate();

	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	FTimerHandle RotateTimer;

	//void TRunRotate();

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
};

