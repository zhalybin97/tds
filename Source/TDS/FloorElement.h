// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/StaticMesh.h"
#include "FloorElement.generated.h"


UCLASS()
class TDS_API AFloorElement : public AActor
{
	GENERATED_BODY()

private:
	AFloorElement* Left;
	AFloorElement* Right;
	AFloorElement* Top;
	AFloorElement* Down;


public:
	TArray<AFloorElement*> neighbors;
	
public:	
	// Sets default values for this actor's properties
	AFloorElement();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
